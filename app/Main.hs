module Main where
import Control.Monad.Trans.State.Lazy
import StatefulCommands (simulate)
import Commands (fixNamesFs)
import Types

main :: IO Types.Navigation
main = execStateT simulate [fixNamesFs "" Types.fs]
