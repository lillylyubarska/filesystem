{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE MultiWayIf #-}
module Commands (rm, cd, fixNamesFs, touch, mkDir, mv, cp) where
import Prelude.Unicode
import Data.Maybe
import Control.Applicative
import Data.List.Split (splitOn)
import Types (Node(..), Navigation)
import Control.Monad (join, liftM2)
import Control.Monad.State.Lazy
import Control.Monad.Writer.Lazy
import Debug.Trace
import Control.Monad.Cont (callCC, runCont)
import Data.List (find, isSuffixOf, isInfixOf, isPrefixOf, intercalate)

--UTILS
delimiter :: String
delimiter = "/"

rootToken :: String
rootToken = delimiter

currentToken :: String
currentToken = "."

parentToken :: String
parentToken = ".."

emptyToken :: String
emptyToken = ""

getName ∷ Node →  String
getName (Folder x xs) = x
getName (File x) = x


(.&&.) :: (a -> Bool) -> (a -> Bool) -> (a -> Bool)
(.&&.) f g a = (f a) && (g a)

splitPath :: String -> [String]
splitPath path = filter (/= emptyToken) $ splitOn delimiter path

getAbsPath :: String -> String
getAbsPath path = intercalate delimiter $
     filter ((/= emptyToken)
    .&&. (/= parentToken)
    .&&. (/= currentToken)) $ splitPath path


fixName :: String -> String -> String
fixName "/" path = delimiter ++ name where name = last $ splitOn delimiter path
fixName parent path = parent ++ delimiter ++ name where name = last $ splitOn delimiter path

normalizeFs :: Navigation -> Navigation
normalizeFs [] = error "Navigation cannot be empty"
normalizeFs navigation@[node] = navigation
normalizeFs xs = foldl (\yss current -> case current of
      Folder name children -> yss ++ [Folder name $ getNewChildren name children $ last yss]
      File name -> [File name] ++ yss) [first] rest where first = head xs
                                                          rest = tail xs
                                                          getNewChildren name children y =
                                                                filter (\x -> getName x /= getName y) children ++ [y]

fixNamesFs ∷ String → Node → Node
fixNamesFs parent (File name) = File $ fixName parent name
fixNamesFs parent (Folder name xs) = let newName = fixName parent name in
        Folder newName (map (fixNamesFs newName) xs)


findByPredicate ∷ (String → Bool) → Node -> Maybe (Node)
findByPredicate predicate (File a) = if (predicate a) then  Just (File a) else Nothing
findByPredicate predicate (Folder a xs) = if (predicate a) then  Just (Folder a xs)
                                             else join (find (≢ Nothing) (map (findByPredicate predicate) xs))


getFirstToken :: String -> String
getFirstToken path = case splitPath path of
                              [] -> ""
                              value ->  head value

getLastToken :: String -> String
getLastToken path = case splitPath path of
                              [] -> ""
                              value ->  last value


getRestPath :: String -> String
getRestPath path = case splitPath path of
                              [] -> ""
                              value ->  intercalate delimiter $ tail value

getPrecedingPath :: String -> String
getPrecedingPath path = case splitPath path of
                              [] -> ""
                              value ->  intercalate delimiter $ init value

isSubPath :: String -> String -> Bool
isSubPath subPath path = subPath `isInfixOf` path


descent :: (Node -> Node) -> String -> Navigation -> Navigation
descent f "" navigation@(x:xs) = f x:xs
descent f "." navigation@(folder@(Folder name ys):xs) = (f folder:xs)
descent f path navigation@((File name):xs) = navigation
descent f path navigation@(folder@(Folder name ys):xs) = (`runCont` id) $ do
      let firstToken = getFirstToken path
      let restPath = if name == rootToken
                  && firstToken /= currentToken then path else getRestPath path
      let newChildren = map (\y -> head $ descent f restPath (y:folder:xs)) ys
      newNav <- callCC $ \exit -> do
          when (firstToken == parentToken ) (exit ((f $ Folder name newChildren):xs))
          newNav' <- callCC $ \exit1 -> do
              when (firstToken == currentToken) (exit1 (descent f (getRestPath path) navigation))
              return (f (Folder name newChildren):xs)
          return newNav'
      return newNav

cd :: String -> Navigation -> Navigation
cd "" navigation = navigation
cd "." navigation = navigation
cd path navigation@((File name):xs) = if isSubPath (getFirstToken path) name then navigation else xs
cd path navigation@(folder@(Folder name ys):xs) = (`runCont` id) $ do
      let firstToken = getFirstToken path
      let restPath = if name == rootToken
            && firstToken /= currentToken then path else getRestPath path
      newNav <- callCC $ \exit -> do
          when (firstToken == parentToken ) (if xs == [] then error "Wrong path"
               else exit $ cd restPath xs)
          newNav' <- callCC $ \exit1 -> do
              when (firstToken == currentToken) (exit1 $ cd restPath navigation)
              let next = find (\x -> isSubPath (getFirstToken restPath) (getName x)) ys
              let res = case next of
                      Nothing -> navigation
                      Just x -> if restPath /= emptyToken then cd restPath (x:navigation) else (navigation)
              return res
          return newNav'
      return newNav

rmHelper :: String -> Node -> Node
rmHelper "." _ = error "Illegal argument"
rmHelper path file@(File x) = file
rmHelper path folder@(Folder a xs) = Folder a newChildren
                                where absPath = getAbsPath path
                                      newChildren = filter (\x -> not $ absPath `isSuffixOf` (getName x)) xs

--COMMANDS
rm :: String -> Navigation -> Navigation
rm path nav = normalizeFs $ descent (rmHelper path) path nav



insertHelper :: String -> Node -> Node -> Node
insertHelper path node file@(File x) = file
insertHelper path node folder@(Folder a xs) = Folder a $ newChildren xs
                                where absPath = getAbsPath path
                                      newChildren children = if absPath `isPrefixOf` a
                                          || absPath == "" then children ++ [node] else children

touch :: String -> Navigation -> Navigation
touch path nav = normalizeFs $ descent (insertHelper path (File name)) path nav where
  name = getLastToken path
  filePath = getPrecedingPath path


mkDir :: String -> Navigation -> Navigation
mkDir path nav = normalizeFs $ descent (insertHelper filePath (Folder name [])) filePath nav where
  name = getLastToken path
  filePath = getPrecedingPath path

findNode :: String -> Navigation -> Maybe Node
findNode fullName nav = case cd (getPrecedingPath fullName) nav of
                            [] -> Nothing
                            (Folder a children):xs ->
                                  find (\x -> getName x `isInfixOf` (getLastToken fullName)) children

cp :: String -> String-> Navigation -> Navigation
cp srcPath destPath nav = let maybeNode = findNode srcPath nav in case maybeNode of
                             Nothing -> nav
                             Just node -> normalizeFs $ descent (insertHelper destPath node) destPath nav

mv :: String -> String-> Navigation -> Navigation
mv srcPath destPath nav = rm srcPath $ cp srcPath destPath nav





