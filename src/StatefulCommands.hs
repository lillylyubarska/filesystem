{-# LANGUAGE UnicodeSyntax #-}
module StatefulCommands (simulate) where
import Prelude.Unicode
import Control.Monad.Trans.State.Lazy (StateT, modify, get)
import Control.Monad.Trans.Class (lift)
import Types (Node, fs, Navigation)
import Commands (cd, rm, touch, mkDir, mv, cp)

rmState :: String -> StateT (Navigation) IO ()
rmState path = modify (rm path)

touchState :: String -> StateT (Navigation) IO ()
touchState path = modify (touch path)

mkDirState :: String -> StateT (Navigation) IO ()
mkDirState path = modify (mkDir path)

cdState :: String -> StateT (Navigation) IO ()
cdState path = modify (cd path)

mvState :: String -> String -> StateT (Navigation) IO ()
mvState srcPath destPath = modify (mv srcPath destPath)

cpState :: String -> String -> StateT (Navigation) IO ()
cpState srcPath destPath = modify (cp srcPath destPath)

printState :: StateT (Navigation) IO ()
printState = do  x <- get
                 lift $ print $ show x
                 lift (print $ "DEPTH " ++ (show $ length x))

simulate ∷ StateT (Navigation) IO ()
simulate = do
    cdState "./data"
    printState
    mkDirState "../trash"
    printState
    mvState "./kursach/porn.avi" "/"
    printState


