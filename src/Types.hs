{-# LANGUAGE UnicodeSyntax #-}
module Types (Node(..), fs, Navigation) where
import Prelude.Unicode

data Node = File String | Folder String [Node]  deriving (Show, Eq)

type Navigation = [Node]

fs ∷ Node
fs = Folder "/" [File "config.ini", File "bla.mp3", File "porn.avi", Folder "data" [ Folder "kursach" [ File "porn.avi"]]]
